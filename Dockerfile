FROM ubuntu:16.04
ENV TERM screen

RUN locale-gen en_US.UTF-8
RUN export LANG=en_US.UTF-8
RUN export LC_TYPE=en_US.UTF-8
RUN export LC_ALL=en_US.UTF-8
RUN echo -e "LANG=en_US.UTF-8\nLC_TYPE=en_US.UTF-8\n" > /etc/environment
RUN apt-get update &&\
    apt-get install -y software-properties-common &&\
    apt-add-repository ppa:nginx/stable &&\
    apt-add-repository ppa:ondrej/php &&\
    apt-add-repository ppa:brightbox/ruby-ng &&\
    apt-key update

RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 4F4EA0AAE5267A6C &&\
    apt-get update

RUN apt-get install -y ntp
COPY ./conf/ntp.conf /etc/ntp.conf
RUN sed -i "s/Etc\/UTC/Europe\/Amsterdam/" /etc/timezone
RUN dpkg-reconfigure --frontend noninteractive tzdata

RUN apt-get install -y curl wget vim
RUN apt-get install -y nginx php7.0-fpm \
    php7.0-sqlite \
    php7.0-redis \
    php7.0-mysql \
    php7.0-imagick \
    php7.0-gd \
    php7.0-curl \
    php-pear \
    php7.0-xdebug \
    php7.0-dev \
    php-gettext \
    php7.0-mcrypt \
    php7.0-mbstring \
    php7.0 \
    php7.0-xml \
    php7.0-igbinary \
    php7.0-yaml \
    php7.0-soap \
    libyaml-dev \
    git \
    zsh \
    zip \
    unzip \
    build-essential \
    libsqlite3-dev \
    ruby2.2 \
    ruby2.2-dev \
    npm

# ZSH
RUN cd $HOME && git clone git://github.com/robbyrussell/oh-my-zsh.git ~/.oh-my-zsh
RUN cp ~/.oh-my-zsh/templates/zshrc.zsh-template ~/.zshrc
RUN chsh -s 'which zsh'
RUN git clone git://github.com/sandstorm/oh-my-zsh-flow3-plugin.git ~/.oh-my-zsh/custom/plugins/flow
RUN sed -i "s/plugins=(git)/plugins=(git flow)/" ~/.zshrc
RUN sed -i "s/# DISABLE_AUTO_TITLE=\"true\"/DISABLE_AUTO_TITLE=\"true\"/" ~/.zshrc

#RUN pecl install yaml
#RUN pecl install igbinary

# Ensure that PHP5 FPM is run as root.
RUN sed -i "s/user = www-data/user = root/" /etc/php/7.0/fpm/pool.d/www.conf
RUN sed -i "s/group = www-data/group = root/" /etc/php/7.0/fpm/pool.d/www.conf
RUN sed -i "s/listen = 127.0.0.1:9000/listen = \/var\/run\/php5-fpm.sock/" /etc/php/7.0/fpm/pool.d/www.conf
RUN sed -i "s/;listen.owner = www-data/listen.owner = root/" /etc/php/7.0/fpm/pool.d/www.conf
RUN sed -i "s/;listen.group = www-data/listen.group = root/" /etc/php/7.0/fpm/pool.d/www.conf
RUN sed -i "s/;listen.mode = 0660/listen.mode = 0660/" /etc/php/7.0/fpm/pool.d/www.conf

# Pass all docker environment
RUN sed -i '/^;clear_env = no/s/^;//' /etc/php/7.0/fpm/pool.d/www.conf

# Get access to FPM-ping page /ping
RUN sed -i '/^;ping\.path/s/^;//' /etc/php/7.0/fpm/pool.d/www.conf
# Get access to FPM_Status page /status
RUN sed -i '/^;pm\.status_path/s/^;//' /etc/php/7.0/fpm/pool.d/www.conf

# Prevent PHP Warning: 'xdebug' already loaded.
# XDebug loaded with the core
RUN sed -i '/.*xdebug.so$/s/^/;/' /etc/php/7.0/mods-available/xdebug.ini

# Copy config
COPY ./conf/nginx.conf /etc/nginx/
COPY ./conf/php.ini /etc/php/7.0/fpm/conf.d/40-custom.ini

# Link php7.0 for cli
RUN ln -sfn /usr/bin/php7.0 /etc/alternatives/php

RUN gem install mailcatcher --no-rdoc --no-ri
RUN sed -i -e "s/.*sendmail_path =.*/sendmail_path = \/usr\/bin\/env \/usr\/local\/bin\/catchmail --smtp-ip mailcatcher -f address@example\.com/" /etc/php/7.0/fpm/php.ini &&\
    sed -i -e "s/.*sendmail_path =.*/sendmail_path = \/usr\/bin\/env \/usr\/local\/bin\/catchmail --smtp-ip mailcatcher -f address@example\.com/" /etc/php/7.0/cli/php.ini

# Composer
RUN curl -sS https://getcomposer.org/installer | php
RUN mv composer.phar /usr/local/bin/composer

# fix node bindings
RUN apt-get install -y nodejs-legacy
RUN gem install bundler
RUN npm install bower -g
RUN npm install grunt-cli -g
RUN apt-get install -y python-pip python-dev build-essential
RUN pip install --upgrade pip
RUN pip install mkdocs
RUN git config --global push.default simple

RUN wget http://download.gna.org/wkhtmltopdf/0.12/0.12.3/wkhtmltox-0.12.3_linux-generic-amd64.tar.xz
RUN tar xvf wkhtmltox-0.12.3_linux-generic-amd64.tar.xz
RUN mv wkhtmltox/bin/wkhtmlto* /usr/bin
RUN rm -Rf whtmltox/
RUN apt-get install -y libxrender1
RUN apt-get install -y ansible

# Setup volumes
VOLUME ["/var/www", "/var/log", "/etc/nginx/conf.d"]

# expose ports
EXPOSE 80 443 8000

WORKDIR /var/www

# Let's go!
CMD git config --global user.name "$GIT_USER" &&\
    git config --global user.email $GIT_EMAIL &&\
    service php7.0-fpm start &&\
    php-fpm7.0 -R &&\
    nginx
